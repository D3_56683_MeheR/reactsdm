CREATE TABLE emp(
    id INTEGER PRIMARY KEY auto_increment,
    name VARCHAR (30),
    salary FLOAT,
    age int
);
